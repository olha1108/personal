import React, { useEffect, useContext, useState } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import SplashScreen from 'react-native-splash-screen';

import { AuthProvider } from './src/context/AuthContext';
import Routes from './src/navigation/Routes';

// import reducers from './src/store/reducers';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// import AuthStack from './src/navigation/AuthStack';
// import AppStack from './src/navigation/AppStack';

function App() { 
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <AuthProvider>
      <Routes />
    </AuthProvider>
  );
}

export default App;
