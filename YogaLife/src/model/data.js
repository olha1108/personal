export const sliderData = [
  {
    title: 'First Picture',
    image: require('../../assets/images/avatars/yoga-nature-picture.png'),
  },
  {
    title: 'Second Picture',
    image: require('../../assets/images/avatars/yoga-post-man.jpg'),
  },
  {
    title: 'Third Picture',
    image: require('../../assets/images/avatars/yoga-post1.jpg'),
  },
];

export const paidGames = [
  {
    avatar: require('../../assets/images/avatars/yoga-ava1.jpg'),
    name: 'Gloria',
    surname: 'Brown',
    isFree: 'No',
    price: '$25.99',
    id: '1',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-ava2.jpg'),
    name: 'Joan',
    surname: 'Smith',
    isFree: 'No',
    price: '$19.99',
    id: '2',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-ava3.jpg'),
    name: 'John',
    surname: 'Johnson',
    isFree: 'No',
    price: '$29.99',
    id: '3',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-ava4.jpg'),
    name: 'Hanna',
    surname: 'Nikson',
    isFree: 'No',
    price: '$24.99',
    id: '4',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-nature-picture.png'),
    name: 'Sonya',
    surname: 'Rickel',
    isFree: 'No',
    price: '$15.99',
    id: '5',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-post-man.jpg'),
    name: 'Mark',
    surname: 'Zukkerman',
    isFree: 'No',
    price: '$25.99',
    id: '6',
  },
];

export const freeGames = [
  {
    avatar: require('../../assets/images/avatars/yoga-post1.jpg'),
    name: 'Olesya',
    surname: 'Novack',
    isFree: 'Yes',
    id: '1',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-post2.jpg'),
    name: 'Arina',
    surname: 'Gamelton',
    isFree: 'Yes',
    id: '2',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-post3.jpg'),
    name: 'Genry',
    surname: 'Yohanson',
    isFree: 'Yes',
    id: '3',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-post4.jpg'),
    name: 'Ford',
    surname: 'Erikson',
    isFree: 'Yes',
    id: '4',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-post5.jpg'),
    name: 'Pokémon',
    surname: 'Clunney',
    isFree: 'Yes',
    id: '5',
  },
  {
    avatar: require('../../assets/images/avatars/yoga-ava1.jpg'),
    name: 'Pablo',
    surname: 'Borison',
    isFree: 'No',
    id: '6',
  },
];