import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

export const AddPostIcon = ({navigation}) => {

    return (
        <TouchableOpacity 
            activeOpacity={0.5} 
            onPress={() => {
            navigation.navigate('AddPost');
        }}>
            <Image
                source={require('../../assets/images/add.png')}
                style={styles.icon}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
    },
    icon: {
        width: 28,
        height: 28,
    }
})