import React from "react";
import { Text, StyleSheet } from "react-native";
import { THEME_CONSTANTS } from "../helpers/ThemeConstants";
 
export function LinkText({ title, style, onPress, disabled }) {
    return (
        <Text
            style={[styles.text, style, disabled && {opacity: 0.5}]}
            onPress={!disabled ? onPress : null}>
            {title}
        </Text>
    )
}

const styles = StyleSheet.create({
    text: {
        color: THEME_CONSTANTS.LIGHT_GREEN,
        padding: 0,
        fontSize: 16,
        fontWeight: '400',
    }
});