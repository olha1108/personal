import React from "react";
import { View, StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import { SafeAreaView } from "react-native-safe-area-context";

export function ContentWrapper({ children, style = {} }) {
    return (
        <TouchableWithoutFeedback
            onPress={Keyboard.dismiss}>
                <View style={[styles.container, style]}>
                    <SafeAreaView edges={['top']}/>
                        {children}
                    <SafeAreaView edges={['bottom']}/>
                </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        backgroundColor: '#fff',
    }
});