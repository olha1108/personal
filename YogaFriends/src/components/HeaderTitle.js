import React from "react";
import { Text, StyleSheet } from "react-native";

import { THEME_CONSTANTS } from "../helpers/ThemeConstants"; 

export function TitleHeader({ title, style }) {
    return (
        <Text style={[styles.title, style]}>{title}</Text>
    )
}

const styles = StyleSheet.create({
    title: {
        textAlign: 'left',
        fontSize: 38,
        fontWeight: '500',
        color: THEME_CONSTANTS.DARK_BROWN,
    }
})