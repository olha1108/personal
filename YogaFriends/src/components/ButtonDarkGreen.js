import React from "react";
import { StyleSheet, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { THEME_CONSTANTS } from "../helpers/ThemeConstants";

export function ButtonDarkGreen({ title, style, onPress, loading}) {
    return (
        <TouchableOpacity
            style={[styles.container, style]}
            onPress={onPress}
            disabled={loading}
            activeOpacity={0.6}>
                {loading ? (
                    <ActivityIndicator size="small" color="#fff" />
                ) : (
                    <Text style={styles.text}>{title}</Text>
                )}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: THEME_CONSTANTS.DARK_GREEN,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: THEME_CONSTANTS.DARK_GREEN,
        paddingVertical: 16,
    },
    text: {
        fontSize: 20,
        fontWeight: '500',
        color: '#fff'
    },
    disabled: {
        backgroundColor: THEME_CONSTANTS.DISABLED,
    }
})