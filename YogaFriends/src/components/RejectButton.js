import React from "react";
import { StyleSheet, Text, TouchableOpacity, ActivityIndicator } from "react-native";
import { THEME_CONSTANTS } from "../helpers/ThemeConstants";

export function RejectButton({ title, style, onPress, loading}) {
    return (
        <TouchableOpacity
            style={style}
            onPress={onPress}>
            {loading ? (
                <ActivityIndicator size="small" color="#FF6230" />
            ) : (
                <Text style={styles.text}>{title}</Text>
            )}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 16,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: THEME_CONSTANTS.ERROR_COLOR,
    },
    text: {
        fontSize: 20,
        fontWeight: '500',
        color: THEME_CONSTANTS.ERROR_COLOR,
    }
})