import React from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";

import { HeaderTitle } from "./HeaderTitle";
import { isAndroid, isIOS } from "../helpers/utils";
import { LogOut } from "./LogOut";
import { AddPostIcon } from "./AddPostIcon";


export function HeaderWithBackArrow({ navigation, title, style = {}, validate, titleStyle, logout = false, addpost = false }) {
    return (
        <SafeAreaView
            edges={['top']}
            style={[styles.area, isAndroid ? styles.border : null]}>
            <View style={styles.container}>
                <View style={[styles.headerBox, style]}>
                    <View style={styles.titleBox}>
                        <TouchableOpacity
                            activeOpacity={0.4}
                            // onPress={() => navigation.goBack()}>
                            onPress={() => (validate ? validate() : navigation.goBack())}>
                            <Image
                                source={require('../../assets/images/chevron-left.png')}
                                style={styles.arrowIcon}
                            />
                            <HeaderTitle title={title} style={[styles.header, titleStyle]} />
                        </TouchableOpacity>
                    </View>
                    {logout && <LogOut/>}
                    {addpost && <AddPostIcon/>}
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    area: {
        backgroundColor: '#fff',
        paddingTop: 0,
        width: '100%',
        zIndex: 2,
    },
    container: {
        backgroundColor: '#fff',
    },
    headerBox: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    titleBox: {
        flexDirection: 'row'
    },
    header: {
        fontSize: 24,
        marginLeft: 16,
        fontWeight: isIOS === 'ios' ? '600' : 'bold',
        flexShrink: 1,
    },
    arrowIcon: {
        width: 38,
        height: 38
    },
    border: {
        borderBottomWidth: 2,
        borderColor: 'rgba(242, 242, 242, 0.8)',
    }
})