import React, {useState, useRef} from "react";
import { StyleSheet, TextInput, View, Text, Animated } from "react-native";
import { THEME_CONSTANTS } from "../helpers/ThemeConstants";
import { windowWidth, windowHeight } from "../helpers/utils";

import AntDesign from 'react-native-vector-icons/AntDesign';
import { TouchableOpacity } from "react-native-gesture-handler";

export const Input = ({style, ...props }) => {
  const [focus, setFocus] = React.useState(false);
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  };

  const fadeOut = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }

  props && props.value && props.value.length ? fadeIn() : fadeOut();

    return (
      <View style={styles.inputContainer}>
        <Animated.View style={[styles.placeholderTop, {opacity: fadeAnim}]}> 
            <Text
              style={[
                styles.labelColor,
                focus && styles.labelFocus,
                // props.error && styles.inputLabelError
                ]}>
                {props.placeholder}
            </Text>
        </Animated.View>
        <TextInput
          {...props}
          style={[styles.input,
          focus && styles.inputFocus,
          props.error && styles.inputBorderError,
          props.styles, style
        ]}
          numberOfLines={1}
          // placeholder={placeholderText}
          placeholderTextColor={THEME_CONSTANTS.DARK_BROWN}
          onPress={() => setFocus(true)}
          onBlur={() => setFocus(false)}
        />
      </View>
    );
  }
  

  const styles = StyleSheet.create({
    inputContainer: {
      position: "relative",
      marginTop: 5,
      marginBottom: 10,
      width: '100%',
      height: 50,
      borderBottomColor: THEME_CONSTANTS.DARK_BROWN,
      borderBottomWidth: 1,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: '#fff',
    },
    input: {
      padding: 10,
      flex: 1,
      fontSize: 16,
      color: '#333',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#fff',
    },
    inputFocus: {
      borderColor: THEME_CONSTANTS.LIGHT_GREEN,
    },
    labelColor: {
      color: THEME_CONSTANTS.DARK_BROWN,
    },
    labelFocus: {
      color: THEME_CONSTANTS.LIGHT_GREEN,
    },
    inputLabelError: {
        color: THEME_CONSTANTS.ERROR_COLOR,
    },
    inputBorderError: {
        color: THEME_CONSTANTS.ERROR_COLOR,
    },
  });

//  const styles = StyleSheet.create({

//     placeholderTop: {
//       zIndex: 1,
//       position: 'absolute',
//       top: -8,
//       left: 12,
//       height: 20,
//       backgroundColor: '#ffffff',
//       paddingHorizontal: 5,
//     },
//  })



// import React, { useState, forwardRef, useCallback } from 'react';
// import { TextInput, Text, View, TouchableOpacity } from 'react-native';
// import styles from './style';
// import { Colors } from '../../../assets/constants';
// import { LinkIcon, BlueEye, GreyEye } from '../../../assets/images/SVG';

// export const Input = ({
//     label, width, placeholder, isPassword, disabled, hasError, errorText, flex, marginRight, textarea,
//     isLink, onChangeText, value, onBlur, ref
// }) => {
//     const [showPassword, setShowPassword] = useState(false);
//     const [isFocused, focus] = useState(false);
//     const { Primary, Secondary, Accent } = Colors;
//     const { container, labelStyle, inputContainer, inputStyle, disabledInputContainer, errorTextStyle, textareaStyle } = styles;
//     const onFocus = () => focus(true);
//     const offFocus = () => focus(false);

//     const _togglePassword = useCallback(() => { setShowPassword(!showPassword) }, [showPassword]);

//     return (
//         <View style={[container, { width, flex, marginRight }]}>
//             <Text style={labelStyle}>{label}</Text>
//             {isLink
//                 ?
//                 <View onBlur={offFocus} style={disabled ? disabledInputContainer : [inputContainer, { borderBottomColor: hasError ? Accent.emergency : isFocused ? Primary.lightBlue : Secondary.lightGrey, height: !textarea ? 50 : 90, minHeight: 50, justifyContent: 'flex-start', paddingLeft: 15 }]}>
//                     <LinkIcon />
//                     <TextInput
//                         autoCapitalize="none"
//                         textContentType={'oneTimeCode'}
//                         ref={ref} onBlur={onBlur}
//                         multiline={textarea} numberOfLines={4}
//                         onFocus={onFocus} editable={!disabled}
//                         placeholderTextColor={Secondary.lightGrey}
//                         secureTextEntry={isPassword}
//                         placeholder={placeholder}
//                         style={textareaStyle}
//                         placeholderTextColor="#B5B5B5"
//                     />
//                 </View>
//                 :
//                 <View onBlur={offFocus} style={disabled ? disabledInputContainer : [inputContainer, { borderBottomColor: hasError ? Accent.emergency : isFocused ? Primary.lightBlue : Secondary.lightGrey, height: !textarea ? 50 : 90, minHeight: 50, justifyContent: !textarea ? 'center' : 'flex-start' }]}>
//                     <TextInput
//                         autoCapitalize="none"
//                         textContentType={'oneTimeCode'}
//                         value={value}
//                         onChangeText={onChangeText}
//                         multiline={textarea}
//                         numberOfLines={4}
//                         onFocus={onFocus}
//                         editable={!disabled}
//                         placeholderTextColor={Secondary.lightGrey}
//                         secureTextEntry={isPassword && !showPassword}
//                         placeholder={placeholder}
//                         style={inputStyle}
//                         placeholderTextColor="#B5B5B5"
//                     />
//                     {isPassword && <>
//                         {showPassword && <TouchableOpacity onPress={_togglePassword}>
//                             <BlueEye style={{ marginRight: 10 }} />
//                         </TouchableOpacity>}
//                         {!showPassword && <TouchableOpacity onPress={_togglePassword}>
//                             <GreyEye style={{ marginRight: 10, }} />
//                         </TouchableOpacity>}
//                     </>}
//                 </View>
//             }
//             {hasError && <Text style={errorTextStyle}>{errorText}</Text>}
//         </View>
//     )
// }