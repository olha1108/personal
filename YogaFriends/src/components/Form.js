import React from 'react';
import {TextInput as RNTextInput, StyleSheet, View} from 'react-native';

import {Text} from './Text';
import colors from '../constants/colors';

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 5,
  },
  labelText: {
    color: colors.DARK_BROWN,
    fontSize: 14,
    marginBottom: 5,
  },
  textInput: {
    fontSize: 14,
    fontWeight: '500',
    paddingBottom: 12,
  },
  border: {
    height: 1,
    backgroundColor: colors.DARK_BROWN,
  },
  borderError: {
    backgroundColor: colors.error,
  },
  errorText: {
    marginTop: 5,
    color: colors.error,
  },
});

export const TextInput = ({label, errorText, ...rest}) => {
  const borderStyles = [styles.border];

  if (errorText && errorText.length > 0) {
    borderStyles.push(styles.borderError);
  }

  return (
    <View style={styles.inputContainer}>
      <Text style={styles.labelText}>{label}</Text>
      <RNTextInput style={styles.textInput} {...rest} />
      <View style={borderStyles} />
      <Text style={styles.errorText}>{errorText}</Text>
    </View>
  );
};
