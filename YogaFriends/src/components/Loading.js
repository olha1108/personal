import React from "react";
import { View, StyleSheet, ActivityIndicator, Text, Platform } from 'react-native';
import { THEME_CONSTANTS } from "../helpers/ThemeConstants";

export function Loading({loading}) {
    if (!loading) {
        return <View/>;
    }
    return (
        <View style={styles.cover}>
            <View style={styles.container}>
                <ActivityIndicator color={'#603814'} />
                <Text style={styles.text}>Loading...</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    cover: {
        ...StyleSheet.absoluteFill,
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        alignItems: 'center',
        justifyContent: 'center',
    },

    container: {
        backgroundColor: '#fff',
        padding: 20,
        borderRadius: 10,
    },

    text: {
        fontSize: 20,
        fontWeight: Platform.OS = 'IOS' ? '600' : 'bold',
        color: THEME_CONSTANTS.DARK_BROWN,
    }
})