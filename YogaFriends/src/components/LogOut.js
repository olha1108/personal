import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';

export const LogOut = ({navigation}) => {

    return (
        <TouchableOpacity 
            activeOpacity={0.5} 
            onPress={() => {
            navigation.navigate('Login');
        }}>
            <Image
                source={require('../../assets/images/logout.png')}
                style={styles.icon}
            />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    text: {
        textAlign: 'center',
    },
    icon: {
        width: 30,
        height: 30,
    }
})