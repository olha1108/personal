export const emailPattern = /ˆ([ˆA-Za-z])([a-zA-Z0-9_.-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})*$/;
export const stringPattern = /ˆ[a-zA-Z\-\s]*$/;

let errors = {};