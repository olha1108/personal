import { Platform, Dimensions } from "react-native";

export const isIOS = Platform.OS = 'ios';
export const isAndroid = Platform.OS = 'android';
export const windowHeight = Dimensions.get('window').height;
export const windowWidth = Dimensions.get('window').width;

export const withDelay = (ms) => new Promise(resolve => setTimeout(resolve, ms))