import React from 'react';
import {StyleSheet, View, ScrollView, Image, Text, Platform, ImageBackground} from 'react-native';
import {Button} from 'react-native-elements';
import { THEME_CONSTANTS } from '../helpers/ThemeConstants';

// const Home = ({navigation}) => {
//   return (
//     <View style={styles.container}>
//       <ScrollView
        // style={styles.scrollStyle}
        // showsVerticalScrollIndicator={false}
        // contentContainerStyle={styles.scrollContainer}>
//           <Text style={styles.text}>Welcome</Text>
//           <Image
//             source={require('../../assets/images/logo1.png')}
//             style={styles.logo}
//         />
//         <Button
//           title="SIGN UP"
//           buttonStyle={styles.lightButton}
//           containerStyle={styles.buttonContainer}
//           titleStyle={styles.buttonTitle}
//           onPress={() => navigation.navigate('SignUp')}
//         />
//         <Button
//           title="LOG IN"
//           buttonStyle={styles.darkButton}
//           containerStyle={styles.buttonContainer}
//           titleStyle={styles.buttonTitle}
//           onPress={() => navigation.navigate('SignIn')}
//         />
//       </ScrollView>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   imageBackground: {
//     flex: 1,
//     resizeMode: 'cover',
//     justifyContent: 'center',
//   },
//   container: {
//     flex: 1,
//     flexDirection: 'column',
//     backgroundColor: '#ffffff',
//   },
  // scrollStyle: {
  //   height: '100%',
  // },
  // scrollContainer: {
  //   flex: 1,
  //   height: '100%',
  //   justifyContent: 'center',
  // },
//   logo: {
//     width: 259,
//     height: 158,
//     alignSelf: 'center',
//     marginVertical: 45,
//   },
//   text: {
//     paddingBottom: 20,
//     fontSize: 40,
//     fontWeight: Platform.OS = 'IOS' ? '600' : 'bold',
//     color: THEME_CONSTANTS.DARK_GREEN,
//     textAlign: 'center',
//   },
//   buttonContainer: {
//     marginHorizontal: 50,
//     marginVertical: 10,
//   },
//   lightButton: {
//     backgroundColor: THEME_CONSTANTS.LIGHT_GREEN,
//     paddingVertical: 15,
//   },
//   buttonTitle: {
//     color: '#ffffff',
//     marginHorizontal: 20,
//   },
//   darkButton: {
//     backgroundColor: THEME_CONSTANTS.DARK_GREEN,
//     paddingVertical: 15,
//   },
// });

// export default Home;



const styles = StyleSheet.create({
  imageBackground: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  buttonContainer: {
    marginHorizontal: 50,
    marginVertical: 10,
  },
  button: {
    backgroundColor: 'rgba(39, 39, 39, 1)',
  },
  buttonTitle: {
    color: '#FFF',
    marginHorizontal: 20,
  },
  lightButton: {
    backgroundColor: '#FFF',
  },
  lightButtonTitle: {
    color: 'rgba(39, 39, 39, 1)',
    marginHorizontal: 20,
  },
});

const Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ImageBackground 
        source={require('../../assets/back2.jpg')}
        style={styles.imageBackground}>
        <Button
          title="Sign In"
          buttonStyle={styles.button}
          containerStyle={styles.buttonContainer}
          titleStyle={styles.buttonTitle}
          onPress={() => navigation.navigate('SignIn')}
        />
        <Button
          title="Sign Up"
          buttonStyle={styles.lightButton}
          containerStyle={styles.buttonContainer}
          titleStyle={styles.lightButtonTitle}
          onPress={() => navigation.navigate('SignUp')}
        />
      </ImageBackground>
    </View>
  );
};

export default Home;