
import { StyleSheet } from 'react-native';
// import {Styles, Fonts, Colors} from '../../../assets/constants';
// import { isPhone, isTablet } from "react-native-device-detection"

const { Regular, SemiBold, Bold, Medium } = Fonts;
const { Accent, Primary, Secondary } = Colors;

const styles = StyleSheet.create({
    imageBackground: {
      flex: 1,
      resizeMode: 'cover',
      justifyContent: 'center',
    },
    container: {
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#ffffff',
    },
    scrollStyle: {
      height: '100%',
    },
    scrollContainer: {
      flex: 1,
      height: '100%',
      justifyContent: 'center',
    },
    logo: {
      width: 259,
      height: 158,
      alignSelf: 'center',
      marginVertical: 45,
    },
    text: {
      paddingBottom: 20,
      fontSize: 40,
      fontWeight: Platform.OS = 'IOS' ? '600' : 'bold',
      color: THEME_CONSTANTS.DARK_GREEN,
      textAlign: 'center',
    },
    buttonContainer: {
      marginHorizontal: 50,
      marginVertical: 10,
    },
    lightButton: {
      backgroundColor: THEME_CONSTANTS.LIGHT_GREEN,
      paddingVertical: 15,
    },
    buttonTitle: {
      color: '#ffffff',
      marginHorizontal: 20,
    },
    darkButton: {
      backgroundColor: THEME_CONSTANTS.DARK_GREEN,
      paddingVertical: 15,
    },
  });

export default styles;