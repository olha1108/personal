/* eslint-disable react-native/no-raw-text */
import React from 'react';
import {StyleSheet, ScrollView, View, SafeAreaView, Text} from 'react-native';
import {TextInput} from '../components/Form';
import {Button} from '../components/Button';

import colors from '../constants/colors';
import HeaderBack from '../components/HeaderBack'
import {withStore} from '../context/AppContext';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    padding: 20,
  },
  scrollStyle: {
    height: '100%',
  },
  scrollContainer: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  darkButton: {
    backgroundColor: colors.DARK_GREEN,
    paddingVertical: 15,
  }
});

const SignIn = ({navigation, store, syncStore}) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [errors, setErrors] = React.useState({});

  const submit = () => {
    setErrors({});
    try {
      if (email && password) {
        syncStore({...store, loggedIn: true, permissions: {settings: true}});
      }
    } catch (error) {
      setErrors();
    }
  };

  return (
    <View style={styles.container}>
        <SafeAreaView style={styles.header}>
          <HeaderBack
              goBack={() => navigation.goBack()}
          />
          <View style={{ margin: 24 }}>
              <Text>Log In</Text>
          </View>
        </SafeAreaView>
        <ScrollView
          style={styles.scrollStyle}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.scrollContainer}>
        <TextInput
          label="Email Address"
          keyboardType="email-address"
          autoCapitalize="none"
          onChangeText={text => setEmail(text)}
          errorText={errors.email}
        />
        <TextInput
          label="Password"
          secureTextEntry
          autoCapitalize="none"
          onChangeText={text => setPassword(text)}
          // errorText={errors.password || signin?.error?.message}
        />
        <Button onPress={submit}>LOG IN</Button>
      </ScrollView>
    </View>
  );
};

export default withStore(SignIn);
